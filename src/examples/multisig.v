Require Import SCaml.

(* Contract types *)

Record storage :=
  { sto_stored_counter : nat
  ; sto_threshold : nat
  ; sto_keys : list key
  }.

Record transfer :=
  { t_amount : tz
  ; t_dest : contract unit
  }.                    

Record change_keys := 
  { ck_threshold : nat
  ; ck_keys : list key
  }.


Inductive action :=
  | Transfer (t : transfer)
  | Delegate (od : option key_hash)
  | Change_keys (ck : change_keys).

Record payload :=
  { pl_counter : nat
  ; pl_action : action
  }.

Record parameter :=
  { p_payload : payload
  ; p_sigs :  list (option signature)
  }.

Definition main (p : parameter) (s : storage) : SCaml (storage * list operation) :=
  let signature_target :=
      Obj.pack (p_payload p, Contract.address (Contract.self parameter), Global.get_chain_id tt)
  in

  assert (sto_stored_counter s == pl_counter (p_payload p)) ;;

  Return (s, nil).

