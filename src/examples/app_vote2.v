Require Import SCaml.

Require Import Util.

Definition storage_ty := map string int.

Definition parameter_ty := string.

Definition mutez5 : tez.mutez := (5000000 ~Mutez).
Extract Inlined Constant mutez5 => "5.0".

Definition typehint_string := @id.
Extract Inlined Constant typehint_string => "(fun (x : string) -> x)".

Definition vote (name : string)  (storage : storage_ty) :
  SCaml (list operation * storage_ty) :=
  let name := typehint_string string name in
  assert ((Tz mutez5) <= (Global.get_amount tt)) ;;
  match Map.get name storage with
  | Some i => Return (nil, Map.update name (Some (i + (Int 1%Z))) storage)
  | None => fail
  end.

Definition vote_spec2
           (storage: storage_ty)
           (name : string)
           (new_storage : storage_ty)
           (returned_operations : list operation) :=
  (* Preconditions *)
  Map.mem name storage = true /\
  ((Tz mutez5) <= (Global.get_amount tt)) = true /\
  (* Postconditions *)
  returned_operations = nil /\
  (forall name',
      match Map.get name' storage,
            Map.get name' new_storage with
      | Some n, Some n'  =>
        n' = if (name == name')
             then int_plus n (Int 1%Z)
             else n
      | None, None => True
      | _, _ => False
      end).

Lemma vote_correct :
  forall name storage returned_operations new_storage,
    vote name storage = Return (returned_operations, new_storage) <->
    vote_spec2 storage name new_storage returned_operations.
Proof.
  intros.
  unfold vote, vote_spec2.
  unfold typehint_string, id.

  destruct (_ <= _).
  - generalize (map_get_none _ _ storage name). intro Hmem.
    destruct (get _ _) eqn:HeqMem.
    + rewrite return_pair_eq.
      split; intros [ Hops Hupd ]; intuition.
      * rewrite <- Hupd.
        generalize (eqb_eq_correct _ name name'); intro Heq.
        destruct (name == name') eqn:Heqb.
        -- rewrite Heq in *.
           rewrite HeqMem.
           rewrite map_updateeq.
           reflexivity.
        -- rewrite map_updateneq by assumption.
           destruct (get name' storage); intuition congruence.
      * apply map_extensionality.
        intro name'. specialize (H2 name').
        generalize (eqb_eq_correct _ name name'); intros Heq.
        destruct (name == name') eqn:Heqb.
        -- destruct Heq.
           rewrite HeqMem in *.
           rewrite map_updateeq.
           destruct (get name new_storage); intuition congruence.
        -- rewrite map_updateneq by assumption.
           destruct (get name' storage); destruct (get name' new_storage);
             intuition congruence.
    + split; unfold fail; intro H; intuition congruence.
  - split; unfold fail; intro H; intuition congruence.
Qed.

Separate Extraction vote.


(** Try to prove that CoqScaml vote2 has the same semantics as the Michelson version.
 ** However, this requires making a bijection between SCaml types and Mi-Cho-Coq types: n
 **  - translate (Int i : ScamlBasics.int) -> i : Z etc
 ** not sure how interesting it is.
 ** the proof afterwards basically amounts to proving that the
 ** michelson program fulfills its spec as the scamlcoq program is
 ** very close the logical specification.
 ***)

Require Import String.
Require Import Michocoq.syntax Michocoq.macros Michocoq.semantics Michocoq.comparable Michocoq.util.
Require Import ZArith.
(*
 * Import Michocoq.error.
 *)
Require List.
Require Michocoq.tez.
Require Michocoq.map.


Module Test.

Definition parameter_ty : type := string.
Definition storage_ty := map string int.

Module ST : (SelfType with Definition self_type := parameter_ty).
  Definition self_type := parameter_ty.
End ST.

Module vote(C:ContractContext)(E:Env ST C).

  Module semantics := Semantics ST C E. Import semantics.

  Definition vote_michelson : full_contract _ ST.self_type storage_ty :=
    (
      AMOUNT ;;
      PUSH mutez (5000000 ~mutez);;
      COMPARE;; GT;;
      IF ( FAIL ) ( NOOP );;
      DUP;; DIP1 ( CDR;; DUP );; CAR;; DUP;;
      DIP1 (
          GET (i := get_map string int);; ASSERT_SOME;;
          PUSH int (Int_constant 1%Z);; ADD (s := add_int_int);; SOME
        );;
      UPDATE (i := Mk_update string (option int) (map string int) (Update_variant_map string int));;
      NIL operation;; PAIR ).

  (* We need to basically, translate (Int i : ScamlBasics.int) -> i : Z etc *)
  (* need type classes *)
  Parameter s_to_s : data storage_ty -> app_vote2.storage_ty.
  Parameter s_to_s' : app_vote2.storage_ty -> data storage_ty.

  Goal (SCaml (Datatypes.list SCamlBasics.operation * app_vote2.storage_ty) =
        (M (data (list operation) * app_vote2.storage_ty))).
  Proof.
    unfold SCaml.
    simpl data. unfold SCamlBasics.operation.
    reflexivity.
  Qed.

  Theorem vote_correct
          (storage : data storage_ty)
          (param : data parameter_ty)
          (new_storage : data storage_ty)
          (returned_operations : data (list operation))
          (fuel : Datatypes.nat) :
    (fuel >= 42)%nat ->
    eval env vote_michelson fuel ((param, storage), tt) = Return ((returned_operations, new_storage), tt)
    <-> vote param (s_to_s storage) = Return (returned_operations, s_to_s new_storage).
  Proof.
  Admitted.

End vote.
End Test.
