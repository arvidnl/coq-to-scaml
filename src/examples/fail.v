Require Import SCaml.
Require Import SCamlExtraction.

(*
 * let main (action : unit) (storage : unit) : (operation list * unit) = ([], ())
 *)

Definition main (action : unit) (storage : unit) : M (list operation * unit) :=
  fail.

(* Option 1: Separate extraction. Creates an unused datatypes file *)
(*
 * Extraction Inline operation.
 * Separate Extraction main SCaml.
 *)

(* Option 2: no inlining, no need to Open SCaml *)

(*
 * Extraction Inline operation.
 *)

Extraction "fail.ml" main.

