Require Import SCaml.

(*
 * type config =
 *   { title          : string
 *   ; beginning_time : timestamp
 *   ; finish_time    : timestamp
 *   }
 *)


Record config_ty :=
  {
    title : string
    ; beginning_time : timestamp
    ; finish_time    : timestamp
  }.

(*
 * type action =
 *   | Vote of string
 *   | Init of config
 *)

Inductive action : Set :=
  | Vote (s : string)
  | Init (c : config_ty).

(*
 * type storage =
 *   { config     : config
 *   ; candidates : (string, int) map
 *   ; voters     : address set
 *   }
 *)

Record storage_ty :=
  { config     : config_ty
  ; candidates : map string int
  ; voters     : set address
  }.

Definition vote (name : string)  (storage : storage_ty) : SCaml (list operation * storage_ty) :=
  let now_ := Global.get_now tt in
  assert ((now_ >= beginning_time (config storage)) && (finish_time (config storage) > now_)) ;;
  let addr := Global.get_source tt in
  assert (negb (SetM.mem addr (voters storage))) ;;
  let x := match Map.get name (candidates storage) with
           | Some i => i
           | None => Int 0%Z
           end
  in
  Return (nil,
   {|
     config := config storage;
     candidates := Map.update name (Some (x + (Int 1%Z))) (candidates storage);
     voters := SetM.update addr true (voters storage);
   |}).

Program Definition init config :=
  {| config := config ;
     candidates := Map (cons ("No", Int 0) (cons ("Yes", Int 0) nil)) ltac:(repeat constructor);
     (* a type annotation is required here *)
     voters := Set_ (nil : list address) ltac:(repeat constructor)
  |}.

Definition main action storage :=
  match action with
  | Vote name => vote name storage
  | Init config => Return (nil, init config)
  end.

Definition vote_spec
           (storage: storage_ty)
           (name : string)
           (new_storage : storage_ty)
           (returned_operations : list operation) :=
  (* Preconditions *)
  Map.mem name (candidates storage) = true /\
  get_now tt >= beginning_time (config storage) /\
  (finish_time (config storage) > get_now tt) /\
  (* Postconditions *)
  (forall s, Map.mem s (candidates storage) = true <->
        Map.mem name (candidates new_storage) = true) /\
  returned_operations = nil /\
  match (Map.get name (candidates storage)) with
  | Some n1 => match (Map.get name (candidates new_storage)) with
              | Some n2 => n2 = (int_plus n1 (Int 1%Z))
              | None => False
              end
  | None => False end /\
  (forall s, s <> name ->
   match (Map.get name (candidates storage)) with
  | Some n1 => match (Map.get name (candidates new_storage)) with
              | Some n2 => n2 = n1
              | None => False
              end
  | None => True end).



Definition vote_spec2
           (storage: storage_ty)
           (name : string)
           (new_storage : storage_ty)
           (returned_operations : list operation) :=
  (* Preconditions *)
  Map.mem name (candidates storage) = true /\
  get_now tt >= beginning_time (config storage) /\
  (finish_time (config storage) > get_now tt) /\
  (* Postconditions *)
  returned_operations = nil /\
  (forall name',
      match Map.get name' (candidates storage),
            Map.get name' (candidates new_storage) with
      | Some n, Some n'  =>
        n' = if (name =? name')%string
             then int_plus n (Int 1%Z)
             else n
      | None, None => True
      | _, _ => False
      end).


Lemma return_pair_eq :
  forall (A B : Type) (a a' : A) (b b' : B),
    Return (a, b) = Return (a', b') <-> (a = a') /\ (b = b').
Proof.
  intros. split.
  intro. inversion H. intuition.
  intro. inversion H. congruence.
Qed.

Lemma and_both {P Q R : Prop} : (Q <-> R) -> (P /\ Q <-> P /\ R).
Proof.
  intuition.
Qed.

Lemma vote_correct :
  forall name storage returned_operations new_storage,
    vote name storage = Return (returned_operations, new_storage) <->
    vote_spec2 storage name new_storage returned_operations.
Proof.
  intros.
  unfold vote, vote_spec2.

  destruct (_ && _) eqn:HeqTime.
  + destruct (SetM.mem _ _) eqn:HeqMem.
    - simpl.
Admitted.


(* This creates a pretty small file, but which includes empty structs for Z and Pos
 * which are not accepted by SCaml. Possible solution: include a ignore and
 * warning in SCaml for (empty?) modules.
 * *)
(*
 * Extraction "extraction/app_vote.ml" vote.
 *)

(* this stuff works better but creates a lot of files, of only app_vote.ml
 * is needed. on the other hand, this file passes in scaml. *)
Separate Extraction vote.
