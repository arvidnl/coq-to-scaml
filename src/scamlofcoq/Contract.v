Require Import SCamlBasics.

Axiom address : forall A, contract A -> address.
Arguments address [A].

Axiom self : forall A, contract A.
