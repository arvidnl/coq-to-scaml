Require Import SCamlBasics.

(*
 * Module Global.
 *)
  Parameter get_now : unit -> timestamp.
  Parameter get_source : unit -> address.
  Parameter get_amount : unit -> tz.
  Parameter get_chain_id : unit -> chain_id.
(*
 * End Global.
 *)
