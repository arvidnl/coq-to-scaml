Require Export Michocoq.error.

Definition SCaml := M.
Definition fail A := Failed A (Assertion_Failure unit tt).
Arguments fail [A].


(** todo: could use the bind instead *)
Notation "'assert' b ';;' C" :=
  (if b then C else fail)
    (at level 200, b at level 100, C at level 200).
