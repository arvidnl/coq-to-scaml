Require Import SCamlBasics.
Require Import Comparable.
Require Import Util.

Require Import Michocoq.map.
Require Import Michocoq.error.

(*+ Definition of set *)

Definition map (A B : Set) `{Compare A}  := map.map A B cmp.

Definition Map {A B : Set} `{C : Compare A} (l : list (A * B))
           (H : Sorted.StronglySorted (set.lt _ (map_compare A B cmp)) l) : map A B
  := exist _ l H.

Definition mem {A B : Set} `{Compare A} : A -> map A B -> bool :=
  map.mem A B cmp.

Definition get {A B : Set} `{Compare A} : A -> map A B -> option B :=
  map.get A B cmp.

Definition update {A B : Set} `{Compare A} : A -> option B -> map A B -> map A B :=
  map.update A B cmp cmp_eq_correct lt_trans gt_trans.

(** Facts *)


Lemma map_extensionality {A B : Set} `{Compare A} :
  forall m1 m2 : map A B, (forall x : A, get x m1 = get x m2) -> m1 = m2.
Proof. apply (map.extensionality A B cmp cmp_eq_correct lt_trans gt_trans). Qed.

Lemma map_updateeq  {A B : Set} `{Compare A}
  : forall (k : A) (m : map A B ) (v : B),
    get k (update k (Some v) m) = Some v.
Proof. apply map.map_updateeq. Qed.

Lemma map_updateneq  {A B : Set} `{Compare A}
  : forall (k k' : A) (m : map A B ) (v : B),
    k <> k' ->
    get k' (update k (Some v) m) = get k' m.
Proof. apply map.map_updateneq. Qed.

Lemma map_get_none (A B : Set) `{Compare A} :
  forall (m : map A B) (n : A),
    mem n m = match get n m with
              | None => false
              | Some _ => true
              end.
Proof.
  intros m n.
  unfold mem.
  destruct (get n m) eqn:Hget.
  - apply IT_eq. now apply map.map_getmem with b.
  - apply IT_neq. intro Hmem. apply map.map_memget in Hmem as [v Hmem].
    unfold get in Hget.
    congruence.
Qed.
