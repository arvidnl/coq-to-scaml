Require Import String.

Require Import SCamlBasics.
Require Michocoq.comparable.
Require Michocoq.tez.

(* For some testing *)
Require Import ZArith.

Class Compare (A : Set) : Type :=
  {
    cmp : A -> A -> comparison ;
    cmp_eq_correct : (forall a b : A, cmp a b = Eq <-> a = b) ;
    lt_trans : Relations_1.Transitive (@Michocoq.comparable.lt_comp A cmp) ;
    gt_trans : Relations_1.Transitive (@Michocoq.comparable.gt_comp A cmp)
  }.

(** Simple comparable *)

Instance cmpComparable (a : syntax_type.comparable_type) : Compare (Michocoq.comparable.comparable_data a) :=
  {|
    cmp := Michocoq.comparable.compare a ;
    cmp_eq_correct := Michocoq.comparable.compare_eq_iff a ;
    lt_trans := Michocoq.comparable.lt_trans a ;
    gt_trans := Michocoq.comparable.gt_trans a
  |}.

Definition cmp2 (A B : Set) (H : Compare A) (g : B -> A)
    (a b : B) : comparison := cmp (g a) (g b).

Lemma liftCompare :
  forall (A B : Set) (H : Compare A)
         (g : B -> A),
         (forall x y, x = y <-> g x = g y) ->
         Compare B.
Proof.
  intros A B H g Hg.
  apply Build_Compare with (cmp2 A B H g) .
  - intros a b. unfold cmp2. rewrite Hg. apply cmp_eq_correct.
  - intros a b c. unfold cmp2. apply lt_trans.
  - intros a b c. unfold cmp2. apply gt_trans.
Defined.


(** Start recycling comparison from MiChocoq *)

Ltac unwrap := intros [x]; apply x.
Ltac unWrapBij := intros [x] [y]; simpl; intuition congruence.

(** Example for wrapped types *)

(* this is now done automatically.
 *
 * Instance cmpInt : Compare int :=
 *   liftCompare _ _ (cmpComparable ((syntax_type.Comparable_type_simple (syntax_type.int))))
 *               (ltac:(unwrap) : int -> _)
 *               ltac:(unWrapBij).
 *)

(** Example for aliased types *)

(*
 * Instance cmpBool : Compare bool :=
 *   cmpComparable ((syntax_type.Comparable_type_simple (syntax_type.bool))).
 *)

Ltac make_compare_alias typ1 :=
  apply (cmpComparable (syntax_type.Comparable_type_simple typ1)).

(** Automate the search for Compare instances *)

Import List.ListNotations.
Open Scope list_scope.

Ltac iter_list E tac :=
  match E with
    | nil => idtac
    | cons ?a ?B => tac a; iter_list B tac
  end.

Ltac iter_all_syntax_types tac :=
  iter_list [(string, syntax_type.string);
             (nat, syntax_type.nat);
             (int, syntax_type.int);
             (bytes, syntax_type.bytes);
             (bool, syntax_type.bool);
             (tz, syntax_type.mutez);
             (address, syntax_type.address);
             (key_hash, syntax_type.key_hash);
             (timestamp, syntax_type.timestamp)] tac.

(*
 * Goal True.
 *   iter_all_syntax_types
 *     ltac:(fun X => match X with | (?x, ?y) => idtac x y end).
 *   constructor. Qed.
 *)

Ltac make_compare_wrapped typ2 typ1  :=
  eapply (liftCompare _ _ (cmpComparable ((syntax_type.Comparable_type_simple typ1))))
  with (g := (ltac:(unwrap) : typ2 -> _)); unWrapBij.

Ltac make_compare_wrapped_all :=
   iter_all_syntax_types ltac:(fun X => match X with | (?x, ?y) => try make_compare_wrapped x y end); fail.

Ltac make_compare_alias_all :=
   iter_all_syntax_types ltac:(fun X => match X with | (?x, ?y) => try make_compare_alias y end); fail.

Goal (Compare bool).
Proof. make_compare_alias_all. Qed.

Goal (Compare nat).
Proof. make_compare_wrapped_all. Qed.

Hint Extern 5 => make_compare_wrapped_all : typeclass_instances.
Hint Extern 5 => make_compare_alias_all : typeclass_instances.

(*
 * Set Typeclasses Debug.
 *)
Goal (cmp (Nat 0%N) (Nat 0%N) = Eq). reflexivity. Qed.
Goal (cmp true true = Eq). reflexivity. Qed.

(** Ascii **)

Instance cmpAddress : Compare address. typeclasses eauto. Defined.

(** Bool *)

Instance cmpBool : Compare address. typeclasses eauto. Defined.

(** Tezos *)

Instance cmpTz : Compare tz. typeclasses eauto. Defined.

(** Timestamps *)

Instance cmpTimestamp : Compare timestamp. typeclasses eauto. Defined.

(** Strings *)

Instance cmpString : Compare string. typeclasses eauto. Defined.

(** Int *)

Instance cmpInt : Compare int. typeclasses eauto. Defined.

(** Pair *)


(*
 * Instance cmpPair (A B : Set) `{Compare A} `{Compare B} : Compare (A * B) :=
 *   cmpComparable syntax_type.pair
 * 
 * typeclasses eauto. Defined.
 *)


(* We could just defined the class of types that has an equivalent in
michocoq, and then get all comparables. I've started doing that here
but ran out of steam for some reason. *)

(*
 * Class FromMiChoCoq (A : Set) : Type :=
 *   {
 *     fmmc_michocoq_type : syntax_type.type;
 *     fmmc_b : Type;
 *     fmmc_to : A -> fmmc_b
 *   }.
 * 
 * Class FromMiChoCoqComparable (A : Set) : Type :=
 *   {
 *     fmccc_michocoq_comparable_type : syntax_type.comparable_type;
 *     fmccc_b : Type;
 *     fmccc_to : A -> fmccc_b;
 *     lol : comparable.comparable_data fmccc_michocoq_comparable_type = A
 *   }.
 * 
 * Instance fmmc_coerce_comparable {A} `{FromMiChoCoqComparable A} : FromMiChoCoq A :=
 *   {
 *     fmmc_michocoq_type := syntax_type.comparable_type_to_type fmccc_michocoq_comparable_type ;
 *     fmmc_b := fmccc_b ;
 *     fmmc_to := fmccc_to ;
 *   }.
 * 
 * Instance fmccBool : (FromMiChoCoqComparable bool) :=
 *   {
 *     fmccc_michocoq_comparable_type := syntax_type.Comparable_type_simple syntax_type.bool;
 *     fmccc_b := bool;
 *     fmccc_to := id;
 *     lol := eq_refl
 *   }.
 * 
 * Instance foo (A : Set) `{FromMiChoCoqComparable A} : Compare A.
 *   destruct H.
 *   rewrite <- lol0.
 *   apply (cmpComparable fmccc_michocoq_comparable_type0).
 * Defined.
 *)

(*+ Helpers for comparison +*)

Definition cmp_ltb { A : Set } `{Compare A} : A -> A -> bool :=
  fun a b => match cmp a b with | Lt => true | _ => false end.
Definition cmp_gtb { A : Set } `{Compare A} : A -> A -> bool :=
  fun a b => match cmp a b with | Gt => true | _ => false end.
Definition cmp_eqb { A : Set } `{Compare A} : A -> A -> bool :=
  fun a b => match cmp a b with | Eq => true | _ => false end.
Definition cmp_leb { A : Set } `{Compare A} : A -> A -> bool :=
  fun a b => match cmp a b with | Eq | Lt => true | _ => false end.
Definition cmp_geb { A : Set } `{Compare A} : A -> A -> bool :=
  fun a b => match cmp a b with | Eq | Gt => true | _ => false end.

(*+ Notations +*)

Declare Scope scaml_scope.
Delimit Scope scaml_scope with scaml.

Infix "<" := cmp_ltb : scaml_scope.
Infix ">" := cmp_gtb : scaml_scope.
Infix "<=" := cmp_leb : scaml_scope.
Infix ">=" := cmp_geb : scaml_scope.
Infix "==" := cmp_eqb (at level 50) : scaml_scope.

(*+ Facts +*)

Lemma eqb_cmp_correct (A : Set) `{Compare A} :
  forall (x y : A), (if cmp_eqb x y then cmp x y = Eq else cmp x y <> Eq).
Proof.
  intros x y.
  unfold cmp_eqb.
  destruct (cmp x y); congruence.
Qed.

Lemma eqb_eq_correct (A : Set) `{Compare A} :
  forall (x y : A), (if cmp_eqb x y then x = y else x <> y).
Proof.
  intros x y.
  specialize (eqb_cmp_correct _ x y); intro Heq.
  destruct (cmp_eqb x y) eqn:Hcmp.
  now apply cmp_eq_correct.
  intro. apply cmp_eq_correct in H0. congruence.
Qed.
