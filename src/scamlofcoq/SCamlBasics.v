Require Import String.
Require Import ZArith.
Require Michocoq.syntax.

Definition operation := Michocoq.syntax.operation_constant.

(** Integers *)
Definition ocaml_int := Z.

Inductive int := | Int (i : ocaml_int).

Definition int_plus i1 i2 :=
  match i1, i2 with
  | Int i1, Int i2 => Int (i1 + i2)%Z
  end.

Declare Scope scaml_int_scope.
Delimit Scope scaml_int_scope with scaml_int.
Infix "+" := int_plus : scaml_int_scope.

(** Nats *)
Inductive nat := | Nat (n : BinNums.N).

Definition nat_plus n1 n2 :=
  match n1, n2 with
  | Nat n1, Nat n2 => Nat (n1 + n2)
  end.

Declare Scope scaml_nat_scope.
Delimit Scope scaml_nat_scope with scaml_nat.
Infix "+^" := nat_plus (at level 40) : scaml_nat_scope .

(** Addresses *)

Definition address := Michocoq.syntax.address_constant.
Definition Address := Michocoq.syntax.Mk_address.

(*
 * Inductive address := | Address (s : string).
 *)

(** Timestamps *)

(* In michocoq, timestamps are internally Zs. There is a
timestamp_constant inductive that takes a str, but i don't think it is
used.  *)
Inductive timestamp := | Timestamp (i : Z).
(*
 * Definition timestamp := Michocoq.syntax.timestamp_constant.
 * Definition Timestamp := Michocoq.syntax.Mk_timestamp.
 *)


(*
 * Declare Scope scaml_ts_scope.
 * Delimit Scope scaml_ts_scope with scaml_ts.
 * 
 * Parameter ts_geb : timestamp -> timestamp -> bool.
 * Parameter ts_gtb : timestamp -> timestamp -> bool.
 * 
 * Infix ">=" := ts_geb : scaml_ts_scope.
 * Infix ">" := ts_gtb : scaml_ts_scope.
 *)

(** Tezos *)

Require Michocoq.tez.

(*
 * Notation "a '#' b" :=
 *   (((a*1000000) + b)%Z)
 *     (at level 200, b at level 200).
 *)

Notation "n ~Mutez" := (exist _ (int64bv.of_Z n) eq_refl) (at level 100).
(*
 * Notation "n ~Tez" := (exist _ (int64bv.of_Z (n * 1000000)%Z) eq_refl) (at level 100).
 *)

Definition test1 : tez.mutez := (5 ~Mutez) .
(*
 * Definition test2 : tez.mutez := (5 ~Tez) .
 *)

Inductive tz := | Tz (t : tez.mutez).

Definition test3 : tz := Tz (5000000 ~Mutez).

(*
 * Parameter tz_geb : tz -> tz -> bool.
 * Parameter tz_gtb : tz -> tz -> bool.
 * 
 * Infix ">=" := tz_geb : scaml_tz_scope.
 * Infix ">" := tz_gtb : scaml_tz_scope.
 *)

(* Domain specific *)
Definition key := Michocoq.syntax.key_constant.
Definition key_hash := Michocoq.syntax.key_hash_constant.
Definition signature := Michocoq.syntax.signature_constant.
Inductive contract (A : Type) := | Contract.
Definition bytes := string.
Definition chain_id := Michocoq.syntax.chain_id_constant.
