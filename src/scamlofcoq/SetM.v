Require Import SCamlBasics.
Require Import Comparable.

Require Import Michocoq.set.

(*+ Definition of set *)

Definition set (A : Set) `{Compare A}  := set.set A cmp.

Definition Set_ {A : Set} `{C : Compare A} (l : list A) (H : Sorted.StronglySorted (set.lt A cmp) l) : set A
  := exist _ l H.

(** Operations *)

Definition mem {A : Set} `{Compare A} : A -> set A -> bool :=
  set.mem A cmp.

Definition update {A : Set} `{Compare A} :
  A -> bool -> set A -> set A :=
  set.update A cmp cmp_eq_correct lt_trans gt_trans.


(** Tests *)
Require Import String.
Section Test.
  Definition test_set_bool : set bool := Set_ (true::nil) ltac:(repeat constructor).
  Definition test_set_address : set address := Set_ (syntax.Mk_address "foo"%string::nil) ltac:(repeat constructor).
End Test.
