Require Export SCamlBasics.
Require Export SetM.
Require Export Map.
Require Export Global.
Require Export SCamlMonad.
Require Export Comparable.

Require Extraction.
Require ExtrOcamlBasic.
Require ExtrOcamlZInt.

Extract Inlined Constant fail => "assert false".
Extract Inductive M => "" ["dummy" ""].
Extract Constant SCaml "a" => "".
Extraction Inline SCaml.
Extraction Inline fail.

Require Import ZArith.

Extract Inductive int => "SCaml.int" [ "SCaml.Int" ].
Extraction Inline ocaml_int.
Extract Inlined Constant int_plus => "SCaml.(+)".

Definition int_test0 := Int 0%Z.
Definition int_test1 := Int 1%Z.
Definition int_test5 := Int 5%Z.

Extract Inductive tz => "SCaml.tz" [ "SCaml.Tz" ].
(*
 * Definition tz_test := (Tz (5 ~Tez))%Z.
 *)
Definition mtz_test := (Tz (5 ~Mutez))%Z.
(*
 * Extraction tz_test.
 *)
Extraction mtz_test.


Extract Inlined Constant Z.add => "".
Extract Inlined Constant Z.double => "".
Extract Inlined Constant Z.succ_double => "".
Extract Inlined Constant Z.pred_double => "".
Extract Inlined Constant Z.pos_sub => "".
Extract Inlined Constant Pos.add => "(fun a b -> assert false)".
Extract Inlined Constant Pos.pred => "(fun a b -> assert false)".
Extract Inlined Constant Pos.pred_double => "(fun a b -> assert false)".
Extract Inlined Constant Pos.succ => "(fun a b -> assert false)".
Extract Inlined Constant Pos.add_carry => "(fun a b -> assert false)".


Extract Constant SetM.set "'a"  => "SCaml.set".
Extraction Inline SetM.set.
Extract Inlined Constant Set_ => "SCaml.Set".

Extract Constant Map.map "'a" "'b" => "SCaml.map".
Extraction Inline Map.map.
Extract Inlined Constant Map => "SCaml.Map".
(*
 * Extract Inlined Constant Map => "SCaml.Map".
 *)
Extract Inlined Constant Map.mem => "SCaml.Map.mem".
Extract Inlined Constant Map.get => "SCaml.Map.get".
Extract Inlined Constant Map.update => "SCaml.Map.update".

(* not sure how to do:
 *  1. create all comparisons necessary, and then manually erase them all? *)
(* TODO: what about these? *)

Extract Inlined Constant cmpBool => "".
Extract Inlined Constant cmpAddress => "".
Extract Inlined Constant cmpInt => "".
Extract Inlined Constant cmpTz => "".
Extract Inlined Constant cmpTimestamp => "".
Extract Inlined Constant cmpString => "".

(*
 * Extract Inlined Constant compare_bool => "".
 * Extract Inlined Constant compare_address => "".
 * Extract Inlined Constant compare_int => "".
 *)
(* or,
 *  2. throw them away thus:
 * *)
(*
 * Extract Inlined Constant SetM.Set_ => "(fun _ -> SCaml.Set_)".
 *)

(*
 * Extraction test1.
 *)

Extraction SetM.set.
(*
 * Definition test_set_int_type := (SetM.set int).
 * Extraction test_set_int_type.
 *)

(*
 * Fact nil_sorted :
 *   Sorted.StronglySorted (set.lt bool (compare_function bool compare_bool)) nil.
 * constructor.
 * Qed.
 * 
 * Definition test_set_value := Set_ bool compare_bool nil nil_sorted.
 * Extraction test_set_value.
 *)


(*
 * Extract Inductive set => "SCaml.set" [ "SCaml.Set" ].
 *)

Extract Inductive timestamp => "SCaml.timestamp" [ Timestamp ].

(*
 * Extract Inductive address => "SCaml.address" [ "SCaml.Address" ].
 *)
Extract Inlined Constant address => "SCaml.address".
Extract Inlined Constant Address => "SCaml.Address".


Extract Inlined Constant operation => "SCaml.operation".
Extract Inlined Constant negb => "SCaml.not".
Extract Inlined Constant cmp_ltb => "SCaml.(<)".
Extract Inlined Constant cmp_gtb => "SCaml.(>)".
Extract Inlined Constant cmp_leb => "SCaml.(<=)".
Extract Inlined Constant cmp_geb => "SCaml.(>=)".
Extract Inlined Constant cmp_eqb => "SCaml.(==)".
Extract Inlined Constant andb => "SCaml.(&&)".

Extract Inlined Constant SetM.mem => "SCaml.Set.mem".
Extract Inlined Constant SetM.update => "SCaml.Set.update".

Extract Inlined Constant Global.get_now => "SCaml.Global.get_now".
Extract Inlined Constant Global.get_source => "SCaml.Global.get_source".
Extract Inlined Constant Global.get_amount => "SCaml.Global.get_amount".

(** How to handle string **)

(** Option 1: *)
(*
 * Require ExtrOcamlString.
 *)
(* problem: produces chars which are not allowed by scaml *)

(** Option 2: *)
Require Import String.
Extract Inductive string => "string" ["""" "bar"].
Extract Inductive Ascii.ascii => "TODO" ["TODO"].
(* problem: produces chars which are not allowed by scaml *)
(*
 * Extract Constant EmptyString => "".
 *)
