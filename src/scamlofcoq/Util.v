Require Import SCamlMonad.

Lemma return_pair_eq :
  forall (A B : Type) (a a' : A) (b b' : B),
    Return (a, b) = Return (a', b') <-> (a = a') /\ (b = b').
Proof.
  intros. split.
  intro. inversion H. intuition.
  intro. inversion H. congruence.
Qed.

Lemma IT_neq : forall b : bool, ~ b -> b = false.
Proof. intros b H. destruct b.
       unfold is_true in H. unfold Is_true in H. unfold Bool.Is_true in H.
       intuition.
       reflexivity.
Qed.
